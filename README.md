**Final Exam**

Fork this repository and invite me to your repository: ariel.lenis@usalesiana.edu.bo


**Problem 1**

Please take a look to the Problem1.java test file

- You need to fix the concurrence issues


**Problem 2**

Please take a look to the Problem2.java test file
- You need to implement a B-Tree to optimize the `public Word findWord(String word)` method

**Problem 3**

Please take a look to the Problem2.java test file
- You need to implement parallel programming to reduce the processing time