package prog4.finalexam;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

public class Problem1 {
    // Problem 1A, The number of inserted rows must match
    @Test
    public void numberOfInsertedRowsTest() throws InterruptedException {
        final int ROWS_IN_CSV = 333333;
        TestHelper helper = new TestHelper();
        WordTable table = helper.insertAllInTable();
        Assert.assertEquals(ROWS_IN_CSV, table.size());
    }

    // Problem 1B, Every row must have an unique incrementalId value
    @Test
    public void incrementalIdMustBeUniqueTest() throws InterruptedException {
        TestHelper helper = new TestHelper();
        WordTable table = helper.insertAllInTable();

        HashSet<Integer> cache = new HashSet<>();

        for (Word row : table) {
            Assert.assertFalse(cache.contains(row.getIncrementalId()));
            cache.add(row.getIncrementalId());
        }
    }
}
