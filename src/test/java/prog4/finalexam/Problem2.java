package prog4.finalexam;


import org.junit.Assert;
import org.junit.Test;

import java.util.List;

// Problem 2, you must use your B-Tree to search words in the class WordTable
// Problem 3, you must use threads to process the words in parallel in the class WordAlgorithms

public class Problem2 {

    @Test
    public void paragraphToWordFrequencyTest() {
        TestHelper helper = new TestHelper();
        WordTable table = helper.insertAllInTable();
        // Carl Sagan, The Demon Haunted World
        String paragraph = "We’ve arranged a global civilization in which most crucial elements—transportation, communications, and all other industries; agriculture, medicine, education, entertainment, protecting the environment; and even the key democratic institution of voting—profoundly depend on science and technology. We have also arranged things so that almost no one understands science and technology. This is a prescription for disaster. We might get away with it for a while, but sooner or later this combustible mixture of ignorance and power is going to blow up in our faces.";
        List<Word> sortedWords = WordAlgorithms.sortWordsByGlobalFrequency(table, paragraph);

        Assert.assertEquals(64, sortedWords.size());
        Assert.assertEquals("the", sortedWords.get(0).getWord());
        Assert.assertEquals("combustible", sortedWords.get(sortedWords.size()-1).getWord());
    }

    @Test
    public void theLastQuestionToWordFrequencyTest() {
        TestHelper helper = new TestHelper();
        WordTable table = helper.insertAllInTable();
        // The short story the last question was copied 3 times in order to have a good scenario to test
        List<Word> sortedWords = WordAlgorithms.sortWordsByGlobalFrequency(table, helper.retrievePlainTextFile("the-last-question-issac-asimov.txt"));

        Assert.assertEquals(1070, sortedWords.size());
        Assert.assertEquals("the", sortedWords.get(0).getWord());
        Assert.assertEquals("powdering", sortedWords.get(sortedWords.size()-1).getWord());
    }

    @Test
    public void bigExampleFrequencyTest() {
        TestHelper helper = new TestHelper();
        WordTable table = helper.insertAllInTable();
        List<Word> sortedWords = WordAlgorithms.sortWordsByGlobalFrequency(table, helper.retrievePlainTextFile("war-of-worlds-hg-wells.txt"));

        Assert.assertEquals(6735, sortedWords.size());
        Assert.assertEquals("the", sortedWords.get(0).getWord());
        Assert.assertEquals("larboard", sortedWords.get(sortedWords.size()-1).getWord());
    }
}
