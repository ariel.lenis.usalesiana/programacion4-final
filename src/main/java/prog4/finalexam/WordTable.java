package prog4.finalexam;

import java.util.ArrayList;
import java.util.Locale;

public class WordTable extends ArrayList<Word> {

    int counter = 0;

    @Override
    public boolean add(Word word) {
        synchronized (this) {
            word.setIncrementalId(counter++);
            return super.add(word);
        }
    }

    // Problem 2, implement the B-Tree to search for values
    public Word findWord(String word) {
        final String normalizedWord = word.toLowerCase();
        for (Word row : this) {
            if (normalizedWord.equals(row.getWord())) {
               return row;
            }
        }

        return null;
    }
}
