package prog4.finalexam;

import java.util.*;

public class WordAlgorithms {
    public static List<Word> sortWordsByGlobalFrequency(WordTable wordTable, String paragraph) {
        String[] paragraphWords = paragraph.split("[\\s\\t\\n\\r]+");
        List<Word> result = new ArrayList<>();
        HashSet<String> cache = new HashSet<>();

        for (String paragraphWord: paragraphWords) {
            String normalizedWord = normalizeWord(paragraphWord);

            if (!cache.contains(normalizedWord)) {
                cache.add(normalizedWord);
                Word existingWord = wordTable.findWord(normalizedWord);
                if (existingWord != null) {
                    result.add(existingWord);
                }
            }
        }

        result.sort(Comparator.comparingLong(Word::getCount).reversed());
        return result;
    }

    private static String normalizeWord(String paragraphWord) {
        return paragraphWord.replaceAll("[^a-zA-Z0-9'\\-_’]", "").toLowerCase();
    }
}
